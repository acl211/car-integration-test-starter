package org.launchcode.training.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by LaunchCode
 */
@Controller
public class HomeController {

    @RequestMapping(value = "/")
    public String index() {
        //TODO: redirect to /cars
        return "index";
    }

}
